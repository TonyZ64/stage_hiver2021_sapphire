**Type**
.Net Core application using the Blazor Server framework. (C#, HTML, CSS)

**FR**

 **Général**
Application servant d'interface d'outils d'administration de postes. Un administrateur peut s'y authentifier, voir les postes enregistrés avec leurs informations réseaux, puis lancer des procédures stockées SQL, qui se traduisent en commandes système, sur ces postes à distance.

 **Rôles**
Deux types d'utilisateur existent.
- Le rôle User peut uniquement visualiser la liste de clients enregistrés et a l'option de visualiser les détails de chaque client:
- Le rôle Administrateur peut effectuer les mêmes actions que le User mais en plus, dispose d'outils permettant d'administrer chaque client.

**Outils administrateur**
L'interface de l'administrateur lui offre les outils suivant afin d'effectuer des tâches sur ces clients:
 - Désactiver une application
 - Démarrer une application
 - Enregistrer dans le registre
 - Installer l'environnement
 - Désactiver l'agent
 - Mettre en place un starter

Il a également la possibilité d'appliquer les outils sur tout les postes du même Group, afin de pousser et rendre encore plus efficace l'automatisation.

Ces outils sont des procédures stockées dans le serveur SQL contenant également les clients et presque toutes les informations nécessaires à l'exécution des tâches. Au choix d'un outil, l'admin peut actionner cet outil en y précisant les paramàtres manquant pour son exécution.

**Authentification**
La persistence d'authentification et de connexion est réalisée avec Asp.Net.Core.Cookies et n'utilise aucun API. AuthService permet de valider les informations en vérifiant dans la base de données, mais ce sont les classes de type PageModel (Login.cshtml, Login.cshtml.cs, Logout.cshtml, Logout.cshtml.cs) qui permettent de créer le cookie et faire persister la connexion. L'enregistrement n'utilise pas de cookie et une authentification doit être faite suite à l'inscription réussie.

**Base de données**
Un fichier .bak contenu dans le dossier BD permet de restaurer une BD fonctionnant avec l'application. De faux clients peuplent la table Client.
Il y a deux utilisateurs par défaut, un user et un administrator. La visualisation des utilisateurs et l'attribution du rôle administrator se fait dans le gestionnaire de base de données utilisé pour restaurer la copie .bak.
Le mot de passe par défaut est contenu dans le fichier pwd.txt et est le même pour les deux utilisateurs par défaut.
