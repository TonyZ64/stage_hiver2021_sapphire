using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using SapphirePortal.Model.Context;
using Microsoft.AspNetCore.Authentication.Cookies;
using SapphirePortal.ViewModel.Clients;
using SapphirePortal.Model.Service.Authentication;
using SapphirePortal.Model.Service.Clients;
using SapphirePortal.ViewModel.Authentication;
using SapphirePortal.Model.Service.Security;

namespace SapphirePortal
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// Appelé pendant le runtime, configure les services pour l'application afin de faire l'injection de dépendances (transient et scoped)
        /// Configure aussi le lien avec le contexte (BD) et applique d'autres directives comme les cookiepolicies ou l'authentification
        /// Plus d'infos sur Startup.cs https://go.microsoft.com/fwlink/?LinkID=398940
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            /// <summary>
            /// Configures rules for the cookie authentication
            /// </summary>
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddAuthentication(
                CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie();
            /// <summary>
            /// Configures Blazor Server services and app root directory
            /// </summary>
            services.AddRazorPages();
            services.AddServerSideBlazor();
            var rootDir = Configuration.GetSection("ApplicationStructure:RootDirectory");
            services.Configure<RazorPagesOptions>(options => options.RootDirectory = rootDir.Value);
            
            ///<summary>
            /// Configures database context (SQL)
            /// </summary>
            var conn = Configuration.GetSection("ConnectionStrings:SapphireDB");
            services.AddDbContext<SapphireDbContext>(options => options.UseSqlServer(conn.Value));
            ///<summary>
            /// Configures cookie accessors
            /// </summary>
            services.AddHttpContextAccessor();
            services.AddScoped<HttpContextAccessor>();
            services.AddHttpClient();
            services.AddScoped<HttpClient>();
            /// <summary>
            /// Initializes services and classes needed, to be used with dependency injection.
            /// </summary>
            services.AddTransient<IClientService, ClientService>();
            services.AddTransient<IClientTaskService, ClientTaskService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAuthService, AuthService>();
            services.AddScoped<RegisterViewModel>();
            services.AddScoped<ClientViewModel>();
            services.AddScoped<ClientToolsViewModel>();
            services.AddScoped<TasksListService>();
            services.AddScoped<CryptoService>();
        }
        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseRouting();

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
            
        }
    }
}
