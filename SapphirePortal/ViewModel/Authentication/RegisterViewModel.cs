﻿using System;
using SapphirePortal.Model.Service.Authentication;

namespace SapphirePortal.ViewModel.Authentication
{
    public class RegisterViewModel
    {
        private readonly IAuthService _authService;
        public RegisterViewModel(IAuthService authenticationService)
        {
            _authService = authenticationService ?? throw new ArgumentNullException(nameof(authenticationService));
        }
        /*Registers user in service
         */
        public void RegisterUser(string email, string password)
        {
            _authService.RegisterUserAsync(email, password);
        }
        /* Checks if a user is registered, by email.
         */
        internal bool UserIsRegistered(string email)
        {
            return !_authService.ValidateRegisterInfo(email);
        }
    }
}
