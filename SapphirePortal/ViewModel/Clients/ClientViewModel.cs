﻿using SapphirePortal.Model.Entity;
using SapphirePortal.Model.Service.Clients;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SapphirePortal.ViewModel.Clients
{
    
    public class ClientViewModel
    {
        private readonly IClientService _objClient;
        /* Uses client service
         */
        public ClientViewModel(IClientService objClient)
        {
            this._objClient = objClient;
        }
        /* Gets client list
         */
        public Task<List<Client>> GetClientList()
        {
            return Task.FromResult(_objClient.GetAllClients());
        }
        /* Gets client details, by id
         */
        public Task<Client> Details(int id)
        {
            return Task.FromResult(_objClient.GetClientData(id));
        }
    }
}
