﻿using SapphirePortal.Model.Entity;
using SapphirePortal.Model.Service.Clients;

namespace SapphirePortal.ViewModel.Clients
{
    public interface IClientTaskService
    {
        public void PerformTask(Client client, string processname, bool applyToGroup);
        public void DeactivateAgentTask(Client client);
        public void SetRegistryTask(Client client, int hklm, string path, string key, string value);
        public void SetStarterTask(Client client, string dataStringIndex, string pathFilenameAg);
    }
    public class ClientToolsViewModel
    {
        private readonly IClientTaskService _taskService;
        private readonly TasksListService _taskListService;
        /* To work, tools need task and tasklist service
         */
        public ClientToolsViewModel(IClientTaskService taskService, TasksListService taskListService)
        {
            this._taskService = taskService;
            this._taskListService = taskListService;
        }
        /* Methods that return lists needed
         */
        public string[] GetToolNameList() { return _taskListService.GetToolNameList(); }
        public string[] GetToRunAppList() { return _taskListService.GetToRunAppList(); }
        public string[] GetToKillAppList() { return _taskListService.GetToKillAppList(); }
        public string[] GetToSetEnvList() { return _taskListService.GetToSetEnvList(); }

        /* Methods that make calls to the tasks service
         */
        public void DeactivateAgentTask(Client client)
        {
            _taskService.DeactivateAgentTask(client);
        }
        public void RunTask(Client client, string env, bool applyToGroup)
        {
            _taskService.PerformTask(client, env, applyToGroup);
        }
        public void SetStarterTask(Client client, string dataInfo, string arg)
        {
            _taskService.SetStarterTask(client, dataInfo, arg);
        }
        public void SetRegistryTask(Client client, string path, string key, string value, int hklm)
        {
            _taskService.SetRegistryTask(client, hklm, path, key, value);
        }
    }
}