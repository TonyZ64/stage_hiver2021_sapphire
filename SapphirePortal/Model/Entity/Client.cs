﻿using System;

namespace SapphirePortal.Model.Entity
{
    public class Client
    {
        /*All the infos a client contains
         */
        public int IClientId { get; set; }
        public string MasterGroup { get; set; }
        public string Group { get; set; }
        public string EmployeeName { get; set; }
        public string Room { get; set; }
        public string Domain { get; set; }
        public string MachineName { get; set; }
        public string Ip { get; set; }
        public string Environment { get; set; }
        public string AgentVersion { get; set; }
        public DateTime Lastupddttm { get; set; }
    }
}
