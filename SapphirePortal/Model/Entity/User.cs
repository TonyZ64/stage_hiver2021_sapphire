﻿using Microsoft.AspNetCore.Identity;

namespace SapphirePortal.Model.Entity
{
    public class User 
    {
        /*Model used for authentification
         */
        [PersonalData]
        public string Email { get; set; }
        [PersonalData]
        public string PasswordHash { get; set; }
        [PersonalData]
        public string Role { get; set; }
        //todo
        [PersonalData]
        public bool IsAuthenticated { get; set; }
    }
}
