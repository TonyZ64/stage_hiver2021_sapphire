﻿using Microsoft.EntityFrameworkCore;
using SapphirePortal.Model.Entity;

namespace SapphirePortal.Model.Context
{
    public partial class SapphireDbContext : DbContext
    {
        public SapphireDbContext(DbContextOptions<SapphireDbContext> options): base(options) { }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<User> User { get; set; }
        /*Creates key references for future get request
        */
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>(entity => {
                entity.HasKey(e => e.IClientId);
            });
            modelBuilder.Entity<User>(entity => {
                entity.HasKey(e => e.Email);
            });
            //
            OnModelCreatingPartial(modelBuilder);
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
