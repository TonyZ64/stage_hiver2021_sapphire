﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace SapphirePortal.Model.Service.Security
{
    public class CryptoService
    {
        /*Encrypts into AES using keys. One way encryption, for register and login validation
         */
        public static byte[] EncryptStringToBytes_Aes(string plainText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");
            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException("iv");
            byte[] encrypted;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = key;
                aesAlg.IV = iv;

                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }
        /*STATIC KEYS : TODO : FIND A BETTER WAY TO STORE KEYS FOR ENCRYPTION, MAYBE SQL TABLE?
         */
        public static byte[] GetKey()
        {
            return Encoding.ASCII.GetBytes("e0c50a797d8f00dfe0c50a797d8f00df");
        }
        public static byte[] GetIv()
        {
            return Encoding.ASCII.GetBytes("e0c50a797d8f00df");
        }

    }
}