﻿using SapphirePortal.Model.Entity;
using System.Collections.Generic;
namespace SapphirePortal.Model.Service.Clients
{
    public interface IClientService
    {
        public List<Client> GetAllClients();
        public Client GetClientData(int id);
    }
}
