﻿using Microsoft.EntityFrameworkCore;
using SapphirePortal.Model.Context;
using SapphirePortal.Model.Entity;
using SapphirePortal.ViewModel.Clients;
using System.Collections.Generic;

namespace SapphirePortal.Model.Service.Clients
{
    public class ClientTaskService : IClientTaskService
    {
        private readonly SapphireDbContext _db;
        protected Dictionary<string, string> taskDict;

        /*Service used to build and execute stored procedures on specified machines in the database*/
        public ClientTaskService(SapphireDbContext db, TasksListService lists)
        {
            _db = db;
            taskDict = lists.GetSqlTaskList();
        }
        /*Builds and executes stored procedure call for RunApp, KillApp and SetEnv*/
        public void PerformTask(Client client, string processName, bool applyToGroup)
        {
            if (applyToGroup) { 
                _db.Database.ExecuteSqlRaw(taskDict[processName], client.Group, null); } 
            else { 
                _db.Database.ExecuteSqlRaw(taskDict[processName], null, client.MachineName); }
        }
        /*Builds and executes stored procedure call to deactivate the Sapphire agent on the client machine*/
        public void DeactivateAgentTask(Client client)
        {
            _db.Database.ExecuteSqlRaw("Task_DeactivateAgent {0},{1},{2}", client.MasterGroup, client.Group, client.MachineName);
        }
        /*Builds and executes stored procedure call to set a new registry key*/
        public void SetRegistryTask(Client client, int hklm, string path, string key, string value)
        {
            _db.Database.ExecuteSqlRaw("Task_SetRegistry {0},{1},{2},{3},{4},{5}", client.Group, client.MachineName, hklm, path, key, value);
        }
        /*Builds and executes stored procedure call to set a program starter*/
        public void SetStarterTask(Client client, string dataStringIndex, string pathFilenameArg)
        {
            _db.Database.ExecuteSqlRaw("Task_SetStarter {0},{1},{2},{3}", client.Group, client.MachineName, dataStringIndex, pathFilenameArg);
        }
    }
}
