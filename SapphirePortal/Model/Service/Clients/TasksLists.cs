﻿using System.Collections.Generic;

namespace SapphirePortal.Model.Service.Clients
{
    public class TasksListService
    {
        public string[] toolNameList = { "Choisir...", "DeactivateAgent", "KillApp", "RunApp", "SetEnvironment", "SetRegistry", "SetStarter" };
        public string[] toKillAppList = { "Choisir...", "Kill Mipacs", "Kill RDS", "Kill Romexis", "Kill Salud" };
        public string[] toRunAppList = { "Choisir...", "Start GPUpdate", "Start Reboot", "Start Mipacs", "Start RDS", "Start Romexis", "Start Salud", "Start SaludAsAdmin" };
        public string[] toSetEnvList = { "Choisir...", "0", "1", "2", "3" };
        Dictionary<string, string> _sqlTaskList = new Dictionary<string, string>();
        
        /*Builds dictionary used for the app*/
        public Dictionary<string, string> GetSqlTaskList()
        {
            //KILL
            _sqlTaskList.Add("Kill Mipacs", "Exe_KillMipacs {0},{1}");
            _sqlTaskList.Add("Kill RDS", "Exe_KillRDS {0},{1}");
            _sqlTaskList.Add("Kill Romexis", "Exe_KillRomexis {0},{1}");
            _sqlTaskList.Add("Kill Salud", "Exe_KillSalud {0},{1}");
            //RUN
            _sqlTaskList.Add("Start GPUpdate", "Exe_GPUpdate {0},{1}");
            _sqlTaskList.Add("Start Reboot", "Exe_Reboot {0},{1}");
            _sqlTaskList.Add("Start Mipacs", "Exe_StartMipacs {0},{1}");
            _sqlTaskList.Add("Start RDS", "Exe_StartRDS {0},{1}");
            _sqlTaskList.Add("Start Romexis", "Exe_StartRomexis {0},{1}");
            _sqlTaskList.Add("Start Salud", "Exe_StartSalud {0},{1}");
            _sqlTaskList.Add("Start SaludAsAdmin", "Exe_StartSaludAsAdmin {0},{1}");
            //SETENV
            _sqlTaskList.Add("0", "Exe_SetEnv0 {0},{1}");
            _sqlTaskList.Add("1", "Exe_SetEnv1 {0},{1}");
            _sqlTaskList.Add("2", "Exe_SetEnv2 {0},{1}");
            _sqlTaskList.Add("3", "Exe_SetEnv3 {0},{1}");
            return _sqlTaskList;
        }
        /*Returns name list of avaiable tools*/
        public string[] GetToolNameList()
        {
            return toolNameList;
        }
        /*Returns list of possible apps to kill*/
        public string[] GetToKillAppList()
        {
            return toKillAppList;
        }
        /*Returns list of possible apps to run*/
        public string[] GetToRunAppList()
        {
            return toRunAppList;
        }
        /*Returns list of possible environments to set*/
        public string[] GetToSetEnvList()
        {
            return toSetEnvList;
        }
    }
}
