﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using SapphirePortal.Model.Context;
using SapphirePortal.Model.Entity;

namespace SapphirePortal.Model.Service.Clients
{
    public class ClientService : IClientService
    {
        private SapphireDbContext _db;
        /*Service used for Client database table interactions (SQL)
         Only read tasks are performed for now (no CUD only R)*/
        public ClientService(SapphireDbContext db)
        {
            this._db = db;
        }
        /*Returns list of all clients*/
        public List<Client> GetAllClients()
        {
            return _db.Client.AsNoTracking().ToList();
        }
        /* Returns a Client based on provided id (iClientID*/
        public Client GetClientData(int id)
        {
            var client = _db.Client.Find(id);
            _db.Entry(client).State = EntityState.Detached;
            return client;
        }
    }
}
