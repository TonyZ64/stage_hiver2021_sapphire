﻿namespace SapphirePortal.Model.Service.Authentication
{
    public interface IAuthService
    {
        public bool ValidateLoginInfo(string username, string passwd);
        public bool ValidateRegisterInfo(string registeringUsername);
        public void RegisterUserAsync(string emailInput, string passwordInput);
    }
}
