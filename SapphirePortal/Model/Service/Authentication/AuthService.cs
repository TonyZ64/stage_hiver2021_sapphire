﻿using System;
using System.Text;
using SapphirePortal.Model.Service.Security;
using SapphirePortal.Model.Entity;
using SapphirePortal.View.Authenticaton;

namespace SapphirePortal.Model.Service.Authentication
{
    public class AuthService : IAuthService
    {
        private readonly IUserService _userService;
        
        /*Service used to make authentication verifications with database
         */
        public AuthService(IUserService userService)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }
        /* Validates Info
         Registers a new user with parameters*/
        public void RegisterUserAsync(string emailInput, string passwordInput)
        {
            if (ValidateRegisterInfo(emailInput))
            {
                var newUser = new User {
                    Email = emailInput,
                    PasswordHash = EncrptUserPassword(passwordInput)

                };
                _userService.AddUser(newUser);
            }
            else
            {
                throw new UserException(UserException.EmailAlreadyTaken);
            }
        }
        /* Checks if user exists
         * Checks if encrypted password given is the same as registered password*/
        public bool ValidateLoginInfo(string emailInput, string passwordInput)
        {
            var dbUser = _userService.GetUserByEmail(emailInput);
            return dbUser != null && ValidatePasswordMatch(EncrptUserPassword(passwordInput), dbUser.PasswordHash);
        }
        /*Password Match
         */
        private static bool ValidatePasswordMatch(string inputPassword, string dbPassword)
        {
            return inputPassword == dbPassword;
        }
        /*Validates that given email does not exist in a user already
         */
        public bool ValidateRegisterInfo(string email)
        {
            return !_userService.UserExists(email);
        }
        /*Calls CryptoService to encrypt given string and return it
         */
        private static string EncrptUserPassword(string passwordInput)
        {
            return Encoding.Default.GetString(CryptoService.EncryptStringToBytes_Aes(passwordInput, CryptoService.GetKey(), CryptoService.GetIv()));
        }

    }
}
