﻿using SapphirePortal.Model.Entity;

namespace SapphirePortal.Model.Service.Authentication
{
    public interface IUserService
    {
        public void AddUser(User client);
        public User GetUserByEmail(string email);
        public bool UserExists(string email);
    }
}