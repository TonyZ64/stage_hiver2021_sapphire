﻿using System;
using SapphirePortal.Model.Entity;
using SapphirePortal.Model.Context;
using System.Linq;

namespace SapphirePortal.Model.Service.Authentication
{
    public class UserService : IUserService
    {
        private SapphireDbContext _db;

        /* Service used for User table interactions in database(SQL)
         */
        public UserService(SapphireDbContext db)
        {
            _db = db;
        }
        /*Adds a user to the database
         */
        public void AddUser(User user)
        {
            //Default role: User
            user.Role = "User";
            _db.User.Add(user);
            _db.SaveChanges();
        }
        /* Retrieves User with provided email
         */
        public User GetUserByEmail(string email)
        {
            try
            {
                return _db.User.Where(x=>x.Email == email).SingleOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
        }
        /* Indicates if user exists with provided email
         */
        public bool UserExists(string email)
        {
            return GetUserByEmail(email) != null;
        }
    }
}

