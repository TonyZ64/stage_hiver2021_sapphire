﻿using System;
using System.Runtime.Serialization;

namespace SapphirePortal.View.Authenticaton
{
    [Serializable]
    internal class UserException : Exception
    {
        //EXCEPTION MESSAGES 
        public const string EmailAlreadyTaken = "This Email is already taken!";
        public const string InvalidEmail = "This Email is invalid!";
        public const string EmptyEmail = "Email is empty!";
        public const string EmptyPassword = "Password is empty!";
        public const string PasswordsNotMatching = "The password confirmation does not match first password!";

        public UserException()
        {
        }

        public UserException(string message) : base(message)
        {
        }

        public UserException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UserException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}