﻿using Microsoft.AspNetCore.Components;
using SapphirePortal.ViewModel.Authentication;

namespace SapphirePortal.View.Authenticaton
{
    public class RegisterView : ComponentBase
    {
        [Inject] protected RegisterViewModel RegisterViewModel { get; set; }
        [Inject] private NavigationManager NavManagr { get; set; }
        protected string NewUserEmail { get; set; }
        protected string NewUserPassword { get; set; }
        protected string ConfirmNewUserPassword { get; set; }
        protected string ValidationMessage { get; set; }
        /*Validates Input, then registers the user and if the registration is successful, the user is brought to registerSuccess page*/
        protected void RegisterToApp()
        {
            try
            {
                ValidateInput();
                RegisterViewModel.RegisterUser(NewUserEmail, NewUserPassword);
                if (RegisterViewModel.UserIsRegistered(NewUserEmail))
                {
                    NavManagr.NavigateTo("/registerSucess");
                }
            }
            catch (UserException e)
            {
                ValidationMessage = e.Message;
            }
        }
        /*Makes first input validation before going further, empty inputs or non-matching passwords*/
        private void ValidateInput()
        {
            if (NewUserEmail == null)
            {
                throw new UserException(UserException.EmptyEmail);
            }

            if (NewUserPassword == null)
            {
                throw new UserException(UserException.EmptyPassword);
            }

            if (!NewUserEmail.Contains("@"))
            {
                throw new UserException(UserException.InvalidEmail);
            }

            if (NewUserPassword != ConfirmNewUserPassword)
            {
                throw new UserException(UserException.PasswordsNotMatching);
            }
        }
    }
}

