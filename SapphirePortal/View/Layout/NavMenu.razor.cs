﻿using Microsoft.AspNetCore.Components;

namespace SapphirePortal.View.Layout
{
    public class NavMenuView : ComponentBase
    {
        protected bool collapseNavMenu = true;
        protected string NavMenuCssClass => collapseNavMenu ? "collapse" : null;
        protected void ToggleNavMenu()
        {
            collapseNavMenu = !collapseNavMenu;
        }
    }
}