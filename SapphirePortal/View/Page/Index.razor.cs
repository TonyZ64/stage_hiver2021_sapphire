﻿using Microsoft.AspNetCore.Components;
using System.Web;

namespace SapphirePortal.View
{
    public class IndexView : ComponentBase
    {

        public string username = "";
        public string password = "";
        public string Encode(string param)
        {
            return HttpUtility.UrlEncode(param);
        }
    }
}