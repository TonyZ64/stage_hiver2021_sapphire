﻿using Microsoft.AspNetCore.Mvc.RazorPages;

namespace SapphirePortal.View.Page
{
    public class HostModel : PageModel
    {
        /*Used for cookie setup, do not touch
         */
        public string UserAgent { get; set; }
        public string IpAddress { get; set; }
        // The following links may be useful for getting the IP Adress:
        // https://stackoverflow.com/questions/35441521/remoteipaddress-is-always-null
        // https://stackoverflow.com/questions/28664686/how-do-i-get-client-ip-address-in-asp-net-core
        public void OnGet()
        {
            UserAgent = HttpContext.Request.Headers["User-Agent"];
            // Note that the RemoteIpAddress property returns an IPAdrress object 
            // which you can query to get required information. Here, however, we pass 
            // its string representation
            IpAddress = HttpContext.Connection.RemoteIpAddress.ToString();
        }
    }
}
