﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SapphirePortal.Model.Service.Authentication;

namespace SapphirePortal.View.Page
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        public string ReturnUrl { get; set; }
        private IUserService _userService;
        private IAuthService _authService;

        public LoginModel(IUserService userService, IAuthService authService)
        {
            this._userService = userService;
            this._authService = authService;
        }
        /*When login action is called inside aspnet core, its cshtml and cshtml.cs file will call this method for cookie setup
         If a cookie was already in use, it is wiped.
        Then, AuthService does its validation, before issuing a new cookie containing its username and its Role*/
        public async Task<IActionResult> OnGetAsync(string paramUsername, string paramPassword)
        {
            string returnUrl = Url.Content("~/");
            try
            {
                await HttpContext
                    .SignOutAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme);
            }
            catch { }

            if (!_authService.ValidateLoginInfo(paramUsername, paramPassword)) return LocalRedirect(returnUrl);
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, paramUsername),
                new Claim(ClaimTypes.Role, _userService.GetUserByEmail(paramUsername).Role),
            };
            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var authProperties = new AuthenticationProperties
            {
                IsPersistent = true,
                RedirectUri = this.Request.Host.Value
            };
            try
            {
                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authProperties);
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
            return LocalRedirect(returnUrl);
        }
    }
}