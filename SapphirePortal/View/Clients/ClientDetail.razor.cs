﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using SapphirePortal.Model.Entity;
using SapphirePortal.ViewModel.Clients;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SapphirePortal.View.Clients
{
    public class ClientDetailView : ComponentBase
    {
        [Inject] protected ClientViewModel ClientViewModel { get; set; }
        [Inject] NavigationManager NavManagr { get; set; }
        [Parameter] public int IClientId { get; set; }
        protected Client selectedClient = new Client();
        /*Gets specified client from it's ViewModel, based on given id
         */
        protected async Task GetClient(int id)
        {
            selectedClient = await ClientViewModel.Details(id);
        }
        /*Navigates back to the ClientBoard component
         */
        protected void ReturnToList()
        {
            NavManagr.NavigateTo("/clients");
        }
    }
}
