﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using SapphirePortal.Model.Entity;
using SapphirePortal.ViewModel.Clients;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SapphirePortal.View.Clients
{
    public class ClientBoardView : ComponentBase
    {
        [Inject]protected ClientViewModel ClientViewModel { get; set; }
        [Inject] NavigationManager NavManagr { get; set; }
        protected List<Client> clientList = new List<Client>();
        /*Get list of clients from it's ViewModel
         */
        protected async Task GetClientList()
        {
            clientList = await ClientViewModel.GetClientList();
        }
        /*Navigates to the client details view of selected client
         */
        protected void ViewClientDetails(int iClientId)
        {
            NavManagr.NavigateTo($"/clients/details/{iClientId}");
        }
        /*Navigates to the client tools view of selected client (Admin only)
         */
        protected void ViewClientTools(int iClientId)
        {
            NavManagr.NavigateTo($"/clients/tools/{iClientId}");
        }
    }
}
