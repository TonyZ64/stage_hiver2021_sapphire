﻿using Microsoft.AspNetCore.Components;
using SapphirePortal.Model.Entity;
using SapphirePortal.ViewModel.Clients;
using System.Threading.Tasks;

namespace SapphirePortal.View.Clients
{
    public class ClientToolsView : ComponentBase
    {
        [Inject] protected ClientToolsViewModel ClientToolsViewModel { get; set; }
        [Inject] protected ClientViewModel ClientViewModel { get; set; }
        [Inject] NavigationManager NavManagr { get; set; }
        [Parameter] public int IClientId { get; set; }
        protected Client selectedClient = new Client();
        protected string ToolName { get{ return activeToolName;} set{activeToolName = value;} }
        protected bool GroupApply { get { return applyToGroup; } set { applyToGroup = value; } }

        /*TODO: Refactor binding functionality
         Also, could make an interface that represent different tool objects*/
        public string activeToolName;
        public bool applyToGroup;
        public string[] toolNameList;
        public string[] toRunProcessList;
        public string[] toKillProcessList;
        public string[] toSetEnvList;
        protected string input1;
        protected string input2;
        protected string input3;
        protected int bitInput;
        /*Gets list of possible tools options
         */
        protected void GetOptionLists()
        {
            toolNameList = ClientToolsViewModel.GetToolNameList();
            toRunProcessList = ClientToolsViewModel.GetToRunAppList();
            toKillProcessList = ClientToolsViewModel.GetToKillAppList();
            toSetEnvList = ClientToolsViewModel.GetToSetEnvList(); ;
        }
        /*Gets client by given ID, from it's ViewModel
         */
        protected async Task GetClient(int id)
        {
            selectedClient = await ClientViewModel.Details(id);
        }
        /*Launches the task to deactivate the agent on selected client, from it's ViewModel
         */
        protected void DeactAgentTool()
        {
            ClientToolsViewModel.DeactivateAgentTask(selectedClient);
        }
        /*Launches the task to kill an app, from it's ViewModel, specifying the client,
         the app to kill and if it is to be applied to machines in the same Group*/
        protected void KillAppTool()
        {
            ClientToolsViewModel.RunTask(selectedClient, input1, GroupApply);
            ResetInput();
        }
        /*Launches the task to run an app, from it's ViewModel, specifying the client,
         the app to run and if it is to be applied to machines in the same Group*/
        protected void RunAppTool()
        {
            ClientToolsViewModel.RunTask(selectedClient, input1, GroupApply);
            ResetInput();
        }
        /*Launches the task to set the environment, from it's ViewModel, specifying the client,
         the environment to set and if it is to be applied to machines in the same Group*/
        protected void SetEnvTool()
        {
            ClientToolsViewModel.RunTask(selectedClient, input1, GroupApply);
            ResetInput();
        }
        /* Launches the task to set a registry key on the machine, from it's ViewModel, using specified parameters
         */
        protected void SetRegisTool()
        {
            ClientToolsViewModel.SetRegistryTask(selectedClient, input1, input2, input3, bitInput);
            ResetInput();
        }
        /*Launches the task to set a program starter, from it's ViewModel, using specified parameters
         */
        protected void SetStarterTool()
        {
            ClientToolsViewModel.SetStarterTask(selectedClient, input1, input2);
            ResetInput();
        }
        /*Navigates back to client board component
         */
        protected void ReturnToList()
        {
            NavManagr.NavigateTo("/clients");
        }
        /*Resets binded input values
         */
        private void ResetInput()
        {
            input1 = "";
            input2 = "";
            input3 = "";
            bitInput = 0;
        }
    }
}
